﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCamera : MonoBehaviour
{
    public float mouseSen = 1;
    public bool clamp = false;
    public Vector2 minmaxX = new Vector2(-30, 60);
    public Vector2 minmaxY = new Vector2(-60, 60);
    private float deadzone = .05f;
    void Start()
    {
        
    }
    void Update()
    {
        Vector2 mouseInput = new Vector2(Input.GetAxis("Mouse X"), -Input.GetAxis("Mouse Y"));
        if(mouseInput.magnitude > deadzone)
        {
            mouseInput *= mouseSen;
            Vector3 rot = transform.eulerAngles;
            rot.x += mouseInput.y;
            rot.y += mouseInput.x;
            if(clamp)
            {
                rot.x = Mathf.Clamp(rot.x, minmaxX.x, minmaxX.y);
                rot.y = Mathf.Clamp(rot.y, minmaxY.x, minmaxY.y);
            }

            transform.rotation = Quaternion.Euler(rot);    
        }
    }
}
