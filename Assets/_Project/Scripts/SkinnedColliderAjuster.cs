﻿using System.Collections;
using UnityEngine;

internal class VertexWeight
{
    public int index;
    public Vector3 localPosition;
    public float weight;

    public VertexWeight(int i, Vector3 p, float w)
    {
        index = i;
        localPosition = p;
        weight = w;
    }
}

internal class WeightList
{
    public Transform transform;
    public ArrayList weights;
    public WeightList()
    {
        weights = new ArrayList();
    }
}

public class SkinnedColliderAjuster : MonoBehaviour
{
    private int[] _skinTriangles;
    private Mesh _skinMesh;
    private MeshCollider _meshCollider;
    private WeightList[] _nodeWeights;
    private SkinnedMeshRenderer _skinMeshRenderer;

    private void Start()
    {
        _skinMeshRenderer = GetComponent<SkinnedMeshRenderer>();
        _skinMesh = new Mesh();

        if (_skinMeshRenderer == null)
        {
            _skinMeshRenderer = gameObject.AddComponent<SkinnedMeshRenderer>();
        }

        _skinTriangles = _skinMeshRenderer.sharedMesh.triangles;

        _meshCollider = GetComponent<MeshCollider>();

        if (_meshCollider == null)
        {
            _meshCollider = gameObject.AddComponent<MeshCollider>();
        }

        Vector3[] cachedVertices = _skinMeshRenderer.sharedMesh.vertices;
        Matrix4x4[] cachedBindposes = _skinMeshRenderer.sharedMesh.bindposes;
        BoneWeight[] cachedBoneWeights = _skinMeshRenderer.sharedMesh.boneWeights;

        _nodeWeights = new WeightList[_skinMeshRenderer.bones.Length];

        for (int i = 0; i < _skinMeshRenderer.bones.Length; i++)
        {
            _nodeWeights[i] = new WeightList
            {
                transform = _skinMeshRenderer.bones[i]
            };
        }

        for (int i = 0; i < cachedVertices.Length; i++)
        {
            BoneWeight bw = cachedBoneWeights[i];
            if (bw.weight0 != 0.0f)
            {
                Vector3 localPt = cachedBindposes[bw.boneIndex0].MultiplyPoint3x4(cachedVertices[i]);
                _nodeWeights[bw.boneIndex0].weights.Add(new VertexWeight(i, localPt, bw.weight0));
            }

            if (bw.weight1 != 0.0f)
            {
                Vector3 localPt = cachedBindposes[bw.boneIndex1].MultiplyPoint3x4(cachedVertices[i]);
                _nodeWeights[bw.boneIndex1].weights.Add(new VertexWeight(i, localPt, bw.weight1));
            }

            if (bw.weight2 != 0.0f)
            {
                Vector3 localPt = cachedBindposes[bw.boneIndex2].MultiplyPoint3x4(cachedVertices[i]);
                _nodeWeights[bw.boneIndex2].weights.Add(new VertexWeight(i, localPt, bw.weight2));
            }

            if (bw.weight3 != 0.0f)
            {
                Vector3 localPt = cachedBindposes[bw.boneIndex3].MultiplyPoint3x4(cachedVertices[i]);
                _nodeWeights[bw.boneIndex3].weights.Add(new VertexWeight(i, localPt, bw.weight3));
            }
        }
    }

    public void CollisionMeshAdjust()
    {
        var vertices = new Vector3[_skinMeshRenderer.sharedMesh.vertexCount];

        foreach (WeightList weightList in _nodeWeights)
        {
            Matrix4x4 matrix = weightList.transform.localToWorldMatrix;

            foreach (VertexWeight vertexWeight in weightList.weights)
            {
                vertices[vertexWeight.index] += matrix.MultiplyPoint3x4(vertexWeight.localPosition) * vertexWeight.weight;
            }
        }

        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = transform.InverseTransformPoint(vertices[i]);
        }

        _skinMesh.vertices = vertices;

        _skinMesh.SetTriangles(_skinTriangles, 0);

        _meshCollider.sharedMesh = _skinMesh;

        _skinMesh.RecalculateBounds();
        _skinMesh.MarkDynamic();
    }
}
