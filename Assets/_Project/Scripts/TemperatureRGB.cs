﻿using UnityEngine;

public class TemperatureRGB : MonoBehaviour
{
    public static Color ColorTemperatureToRGB(int kelvin)
    {

        float temp = kelvin / 100f;

        float red, green, blue;

        if (temp <= 66)
        {

            red = 255;

            green = temp;
            green = (float)(99.4708025861 * Mathf.Log(green) - 161.1195681661);


            if (temp <= 19)
            {

                blue = 0;

            }
            else
            {

                blue = temp - 10;
                blue = (float)(138.5177312231 * Mathf.Log(blue) - 305.0447927307);

            }

        }
        else
        {

            red = temp - 60;
            red = (float)(329.698727446 * Mathf.Pow(red, (float)(-0.1332047592)));

            green = temp - 60;
            green = (float)(288.1221695283 * Mathf.Pow(green, (float)(-0.0755148492)));

            blue = 255;

        }


        return new Color(
            Clamp(red, 0, 255),
            Clamp(green, 0, 255),
            Clamp(blue, 0, 255));

    }


    private static float Clamp(float x, float min, float max )
    {

        if (x < min) { return min; }
        if (x > max) { return max; }

        return x;

    }
}
