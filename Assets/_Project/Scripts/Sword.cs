﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour
{
    public delegate void DestroyHandler();

    public event DestroyHandler OnDestroy;

    private const float ColliderRadius = 0.4f;
    private readonly Vector3 ColliderBox = new Vector3(0.007f, 0.0106f, 0.002f);

    [SerializeField] private float _minRotationDegree;
    [SerializeField] private float _maxRotationDegree;

    [SerializeField] private SkinnedColliderAjuster _skinnedColliderAjuster;
    [SerializeField] private Transform _sword;
    [SerializeField] private Transform _rootBone;
    [SerializeField] private Renderer _swordRenderer;

    [SerializeField] private List<Transform> _boneList;

    [SerializeField] private float _heatAmountOverTime = 15;
    [SerializeField] private float _coolingAmountOverTime = 20;
    [SerializeField] private float _maxHeat = 2f;
    [SerializeField] private float _minHeat = 0.3f;
    [SerializeField] private float _heatLoss = 0.25f;
    [SerializeField] private float _currentHeat = 5f;

    [SerializeField] private float _boneRotationTolerance = 1f;

    private bool _isCooling;
    private bool _isHeating;
    private bool _isFlipped;
    private Color _heatColor = new Color(255, 70, 0);
    private Color _coldColor = new Color(179, 185, 209);
    private Coroutine _coolingSwordCoroutine;
    private Coroutine _heatingSwordCoroutine;
    private Coroutine _swordHeatCoroutine;
    private Hammer _hammer;

    public void Initialize()
    {
        _hammer = FindObjectOfType<Hammer>();
        _hammer.OnHit += GetHit;
        GameManager.Instance.OnUpdateHeatUI += HeatColor;

        _boneList = new List<Transform>();

        _boneList.Add(_rootBone.GetChild(0));
        _boneList.Add(_rootBone.GetChild(0).GetChild(0));
        _boneList.Add(_rootBone.GetChild(0).GetChild(0).GetChild(0));
        _boneList.Add(_rootBone.GetChild(0).GetChild(0).GetChild(0).GetChild(0));
        _boneList.Add(_rootBone.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0));
        _boneList.Add(_rootBone.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0));
        _boneList.Add(_rootBone.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0));
        _boneList.Add(_rootBone.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0));

        if (_boneList.Count == 0)
        {
            Debug.LogError("bones empty");
        }

        foreach (Transform bone in _boneList)
        {
            bone.localEulerAngles = new Vector3(RandomBladeDeformationValue(), 0, 0);
            var boneCollider = bone.gameObject.AddComponent<BoxCollider>();
            boneCollider.size = ColliderBox;
            boneCollider.center = new Vector3(0, 0.003f, 0);
        }
        OnFlipSword();

        _swordHeatCoroutine = StartCoroutine(SwordHeatCoroutine());
        StartCoroutine(LateMeshUpdate());
    }

    IEnumerator LateMeshUpdate()
    {
        for (int i = 0; i < 10; i++)
        {
            yield return null;
        }
        _skinnedColliderAjuster.CollisionMeshAdjust();
    }

    private IEnumerator SwordHeatCoroutine()
    {
        while (gameObject.activeSelf)
        {
            if (_currentHeat > _minHeat)
            {
                _currentHeat -= (_heatLoss * Time.fixedDeltaTime) / 60;
            }
            yield return null;
        }
    }

    public void SetNewBoneRotationTolerance(int toleranceValue)
    {
        _boneRotationTolerance = toleranceValue;
    }

    public void SetHeatValue(float heatValue)
    {
        if (_currentHeat > _maxHeat)
        {
            _currentHeat = _maxHeat;
            return;
        }

        if (_currentHeat < _minHeat)
        {
            _currentHeat = _minHeat;
            return;
        }

        _currentHeat += heatValue;
    }

    public void OnFlipSword()
    {
        _sword.DOLocalRotate(ToggleFlipAngle(), 0.5f, RotateMode.Fast);
    }

    public void OnStarHeating()
    {
        if (_heatingSwordCoroutine == null)
        {
            _isHeating = true;
            _heatingSwordCoroutine = StartCoroutine(HeatCoroutine());
        }
    }

    public void OnStopHeating()
    {
        _isHeating = false;
    }

    public void OnStarCooling()
    {
        if (_coolingSwordCoroutine == null)
        {
            _isCooling = true;
            _coolingSwordCoroutine = StartCoroutine(CoolingCoroutine());
        }
    }
    public void OnStopCooling()
    {
        _isCooling = false;
    }

    public void OnRotateSword(Vector2 mouseInput)
    {
        Vector3 rot = transform.eulerAngles;
        rot.x += mouseInput.x * -3f;
        rot.z += mouseInput.y * -1.5f;
        transform.rotation = Quaternion.Euler(rot);
    }

    public void OnGridingSword(Vector2 mouseInput)
    {

    }

    public void OnChangePosition(Transform newPlace)
    {
        this.transform.position = newPlace.position;
        this.transform.rotation = newPlace.rotation;
    }

    private IEnumerator HeatCoroutine()
    {
        while (_isHeating)
        {
            float heatAmount = _heatAmountOverTime * Time.fixedDeltaTime / 60;

            SetHeatValue(heatAmount);
            yield return null;
        }

        _heatingSwordCoroutine = null;
    }

    private IEnumerator CoolingCoroutine()
    {
        while (_isCooling)
        {
            float heatAmount = _coolingAmountOverTime * Time.fixedDeltaTime / 60;

            SetHeatValue(-heatAmount);
            yield return null;
        }
        _coolingSwordCoroutine = null;
    }

    private Vector3 ToggleFlipAngle()
    {
        _isFlipped = !_isFlipped;

        Vector3 rotation = this._sword.localEulerAngles;
        rotation.y = _isFlipped ? 0 : 180;

        return rotation;
    }

    private float RandomBladeDeformationValue()
    {
        return Random.Range(_minRotationDegree, _maxRotationDegree);
    }

    private void GetHit(List<Transform> boneList, float bendValue)
    {
        if (GameManager.Instance.HardMode)
        {
            var boneRotationA = boneList[0].transform.localEulerAngles;
            var boneRotationB = boneList[1].transform.localEulerAngles;
            var boneRotationC = boneList[2].transform.localEulerAngles;

            var bend = bendValue * (_isFlipped ? 1 : -1) * _currentHeat;

            boneRotationA.x -= bend * 0.4f;
            boneRotationB.x += bend;
            boneRotationC.x += bend * 0.4f;

            boneList[0].transform.localEulerAngles = boneRotationA;
            boneList[1].transform.localEulerAngles = boneRotationB;
            boneList[2].transform.localEulerAngles = boneRotationC;
        }
        else
        {
            foreach (Transform bone in boneList)
            {
                var boneRotation = bone.transform.localEulerAngles;

                boneRotation.x += bendValue * (_isFlipped ? 1 : -1) * _currentHeat;
                bone.transform.localEulerAngles = boneRotation;
            }
        }
        _skinnedColliderAjuster.CollisionMeshAdjust();
    }

    public void DeliverSword()
    {
        int badPoints = 0;
        int goodPoints = 0;

        foreach (Transform bone in _boneList)
        {
            var boneRotationX = bone.transform.localEulerAngles.x;

            if (boneRotationX > _boneRotationTolerance || boneRotationX < -_boneRotationTolerance)
            {
                badPoints++;
            }
            else
            {
                goodPoints++;
            }
        }

        if (goodPoints > badPoints)
        {
            GameManager.Instance.GoodRepaired++;
        }
        else
        {
            GameManager.Instance.BadRepaired++;
        }

        GameManager.Instance.SwordsRepaired++;

        DestroyRoutine();
    }

    private void DestroyRoutine()
    {
        _hammer.OnHit -= GetHit;
        GameManager.Instance.OnUpdateHeatUI -= HeatColor;

        StopCoroutine(_swordHeatCoroutine);
        gameObject.SetActive(false);

        OnDestroy?.Invoke();

        Destroy(this.gameObject);
    }

    private float HeatColor()
    {

        return _currentHeat / _maxHeat;
    }
}
