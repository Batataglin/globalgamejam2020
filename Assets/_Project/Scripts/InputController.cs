﻿using DG.Tweening;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public delegate void MouseButtonClickHandler();
    public delegate void HammeringHandler(Vector3 hitPoint);
    public delegate void SwordRotationHandler(Vector2 rotation);
    public delegate void KeyboardButtonHandler();
    public delegate void ChangeViewModeHandler(Transform position);
    public delegate void DeliverSwordHandler();

    public event HammeringHandler OnHammerig;
    public event MouseButtonClickHandler OnFlipSword;
    public event MouseButtonClickHandler OnRightClick;
    public event ChangeViewModeHandler OnChangeViewMode;
    public event SwordRotationHandler OnRotateSword;
    public event SwordRotationHandler OnGridingSword;
    public event MouseButtonClickHandler OnSetSwordToHeating;
    public event MouseButtonClickHandler OnSetSwordToCooling;
    public event MouseButtonClickHandler OnStopSwordHeating;
    public event MouseButtonClickHandler OnStopSwordCooling;
    public event DeliverSwordHandler OnDeliverSword;

    [SerializeField] private Material _readyToInteract;
    [SerializeField] private Material _notReadyToInteract;
    [SerializeField] private Transform _hitLocation;

    [Header("Camera positions")]
    [SerializeField] private Transform _anvilPosition;
    [SerializeField] private Transform _aligmentPosition;
    [SerializeField] private Transform _furnacePosition;
    [SerializeField] private Transform _grinderPosition;

    [Header("Sword positions")]
    [SerializeField] private Transform _swordAnvilPosition;
    [SerializeField] private Transform _swordAligmentPosition;
    [SerializeField] private Transform _swordFurnacePosition;
    [SerializeField] private Transform _swordCoolingPosition;
    [SerializeField] private Transform _swordHeatingPosition;
    [SerializeField] private Transform _swordGrinderPosition;

    private Camera m_camera;
    private GameManager m_gameManager;

    private float deadzone = .05f;
    private float mouseSen = 1;

    private void Start()
    {
        m_camera = Camera.main;
        m_gameManager = GameManager.Instance;
        Cursor.visible = true;
    }
    private void Update()
    {
        if(m_gameManager.ViewMode == ViewMode.Anvil)
        {
            AnvilViewInputController();
        }
        else if (m_gameManager.ViewMode == ViewMode.AligmentView)
        {
            AligmentlViewInputController();
        } 
        else if (m_gameManager.ViewMode == ViewMode.Furnace)
        {
            FurnaceViewInputController();
        }
        else if(m_gameManager.ViewMode == ViewMode.WheelGrinder)
        {
            GrinderViewInputController();
        }

        ChangeGameMode();
    }

    private void ChangeGameMode()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            m_gameManager.ViewMode = ViewMode.Furnace;
            CameraMovement(_furnacePosition);

            OnChangeViewMode?.Invoke(_swordFurnacePosition);
            GameJamUI.OnForge();
        }
        else
        if (Input.GetKey(KeyCode.Alpha2))
        {
            m_gameManager.ViewMode = ViewMode.Anvil;

            _hitLocation.gameObject.SetActive(true);

            CameraMovement(_anvilPosition);
            OnChangeViewMode?.Invoke(_swordAnvilPosition);
            GameJamUI.OnAnvil();
        }
        else
        if (Input.GetKey(KeyCode.Alpha3))
        {
            m_gameManager.ViewMode = ViewMode.AligmentView;

            _hitLocation.gameObject.SetActive(false);

            CameraMovement(_aligmentPosition);
            OnChangeViewMode?.Invoke(_swordAligmentPosition);
            GameJamUI.OnAlignment();
        }
        else
        if (Input.GetKey(KeyCode.Alpha4))
        {
            m_gameManager.ViewMode = ViewMode.WheelGrinder;
            CameraMovement(_grinderPosition);
            OnChangeViewMode?.Invoke(_swordGrinderPosition);
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            m_gameManager.ViewMode = ViewMode.Anvil;

            _hitLocation.gameObject.SetActive(true);

            CameraMovement(_anvilPosition);
            OnChangeViewMode?.Invoke(_swordAnvilPosition);
            GameJamUI.OnAnvil();

            OnDeliverSword?.Invoke();
        }
    }
    private void CameraMovement(Transform destination)
    {
        OnStopSwordHeating?.Invoke();
        OnStopSwordCooling?.Invoke();

        m_camera.transform.DOMove(destination.position, 1f);
        m_camera.transform.DORotate(destination.eulerAngles, 1f);
    }
    private void AnvilViewInputController()
    {
        HitIndicator();

        if (Input.GetMouseButtonDown(0))
        {
            if (m_gameManager.ViewMode == ViewMode.Anvil)
            {
                OnHammerig?.Invoke(_hitLocation.position);
            }
        }
        else
        if (Input.GetMouseButtonDown(1))
        {
            if (m_gameManager.ViewMode == ViewMode.Anvil)
            {
                OnFlipSword?.Invoke();
            }
        }
    }
    private void AligmentlViewInputController()
    {
        MovementOnAligmentView();
    }
    private void FurnaceViewInputController()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnChangeViewMode?.Invoke(_swordHeatingPosition);
            OnSetSwordToHeating?.Invoke();
            AudioEffectsContoller.Instance.PlayFurnaceSound();
            OnStopSwordCooling?.Invoke();
        }
        else if (Input.GetMouseButtonDown(1))
        {
            OnChangeViewMode?.Invoke(_swordCoolingPosition);
            OnSetSwordToCooling?.Invoke();
            AudioEffectsContoller.Instance.PlayWaterSound();
            OnStopSwordHeating?.Invoke();
        }
    }
    private void GrinderViewInputController()
    {
        //MovementOnGrinderView();
    }
    private void HitIndicator()
    {
        Ray _rayFromMousePosition = m_camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(_rayFromMousePosition, out RaycastHit hit, Mathf.Infinity))
        {
            _hitLocation.position = hit.point;

            if (hit.transform.gameObject.layer == 8)
            {
                _hitLocation.GetComponent<Renderer>().material = _readyToInteract; 
            }
            else
            {
                _hitLocation.GetComponent<Renderer>().material = _notReadyToInteract;
            }
        }

        Debug.DrawLine(_rayFromMousePosition.origin, _rayFromMousePosition.origin + _rayFromMousePosition.direction * 10);
    }
    private void MovementOnAligmentView()
    {
        Vector2 mouseInput = new Vector2(Input.GetAxis("Mouse X"), -Input.GetAxis("Mouse Y"));
        
        if (mouseInput.magnitude > deadzone)
        {
            mouseInput *= mouseSen;

            OnRotateSword?.Invoke(mouseInput);
        }
    }
    private void MovementOnGrinderView()
    {
        Vector2 mouseInput = new Vector2(Input.GetAxis("Mouse X"), -Input.GetAxis("Mouse Y"));

        if (mouseInput.magnitude > deadzone)
        {
            mouseInput *= mouseSen;

            OnGridingSword?.Invoke(mouseInput);
        }


        if (Input.GetMouseButtonDown(0))
        {
            OnFlipSword?.Invoke();
        }
    }
}
