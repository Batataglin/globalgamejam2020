﻿using System.Collections.Generic;
using UnityEngine;

public class Hammer : MonoBehaviour
{
    private const float HitDistance = 1f;

    public delegate void HammerHitHandler(List<Transform> bonesHitted, float bendValue);
    public event HammerHitHandler OnHit;

    [SerializeField] private float _hammerBendValue = 0.3f;

    private LayerMask swordLayer = 1 << 8;
    public List<Transform> _boneTransform = new List<Transform>();


    private bool SingleBone(Vector3 hitPoint)
    {
        Ray rayFromHammer = new Ray(hitPoint - new Vector3(0, -0.5f, 0), Vector3.down);

        _boneTransform.Clear();

        if (Physics.SphereCast(rayFromHammer, 0.4f, out RaycastHit hit, HitDistance, swordLayer))
        {
            if (GameManager.Instance.HardMode)
            {
                _boneTransform.Add(hit.transform.parent);
                _boneTransform.Add(hit.transform);
                _boneTransform.Add(hit.transform.GetChild(0));
            }
            else
            {
                _boneTransform.Add(hit.transform);
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public void OnHammer(Vector3 hitPoint)
    {
        if (SingleBone(hitPoint))
        {
            AudioEffectsContoller.Instance.PlayHammerSound();
            OnHit?.Invoke(_boneTransform, _hammerBendValue);
        }
    }
}
