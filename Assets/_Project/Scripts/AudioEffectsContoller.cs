﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioEffectsContoller : MonoBehaviour
{
    public static AudioEffectsContoller Instance;

    [SerializeField] private AudioClip[] _hammer;
    [SerializeField] private AudioClip _water;
    [SerializeField] private AudioClip _furnace;

    [SerializeField] private AudioSource _audioSource;

    private void Awake()
    {
        Instance = this;
    }

    public void PlayHammerSound()
    {
        _audioSource.clip = _hammer[Random.Range(0, _hammer.Length)];
        _audioSource.Play();
    }

    public void PlayWaterSound()
    {
        _audioSource.clip = _water;
        _audioSource.Play();
    }

    public void PlayFurnaceSound()
    {
        _audioSource.clip = _furnace;
        _audioSource.Play();
    }
}
