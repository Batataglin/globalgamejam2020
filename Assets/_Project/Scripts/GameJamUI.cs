﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameJamUI : MonoBehaviour
{
    public KeyCode disableKey;
    public GameObject root;
    public GameObject anvilNotes;
    public GameObject forgeNotes;
    public GameObject alignNotes;
    public GameObject deliverNotes;

    static private GameJamUI instance;
    void Start()
    {
        instance = this;
        StartCoroutine(HideUi());
    }
    void Update()
    {
        if(Input.GetKeyDown(disableKey))
        {
            root.SetActive(!root.activeSelf);
        }
    }

    private IEnumerator HideUi()
    {
        float time = 10f; 

        while (time > 0)
        {
            time -= Time.fixedDeltaTime;
            yield return null;
        }

        root.SetActive(false);
    }

    public static void DisableAllNotes()
    {
        instance.anvilNotes.SetActive(false);
        instance.forgeNotes.SetActive(false);
        instance.alignNotes.SetActive(false);
    }
    public static void OnAnvil()
    {
        DisableAllNotes();
        instance.anvilNotes.SetActive(true);
    }
    public static void OnForge()
    {
        DisableAllNotes();
        instance.forgeNotes.SetActive(true);
    }
    public static void OnAlignment()
    {
        DisableAllNotes();
        instance.alignNotes.SetActive(true);
    }
}
