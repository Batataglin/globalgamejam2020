﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField] private InputController _inputController;
    [SerializeField] private Hammer _hammer;
    
    private Sword _sword;

    private void Start()
    {
        _inputController.OnHammerig += _hammer.OnHammer;
        _inputController.OnDeliverSword += GameManager.Instance.SpawnSword;
        GameManager.Instance.OnSpawn += OnSpawnSword;
    }

    private void OnSpawnSword(Sword sword)
    {
        _sword = sword;
        _sword.Initialize();

        _inputController.OnFlipSword += _sword.OnFlipSword;
        _inputController.OnChangeViewMode += _sword.OnChangePosition;
        _inputController.OnRotateSword += _sword.OnRotateSword;
        _inputController.OnGridingSword += _sword.OnGridingSword;

        _inputController.OnSetSwordToHeating += _sword.OnStarHeating;
        _inputController.OnStopSwordHeating += _sword.OnStopHeating;

        _inputController.OnSetSwordToCooling += _sword.OnStarCooling;
        _inputController.OnStopSwordCooling += _sword.OnStopCooling;

        _inputController.OnDeliverSword += _sword.DeliverSword;

        _sword.OnDestroy += OnDestroySword;
    }

    public void OnDestroySword()
    {
        _sword.OnDestroy -= OnDestroySword;

        _inputController.OnFlipSword -= _sword.OnFlipSword;
        _inputController.OnChangeViewMode -= _sword.OnChangePosition;
        _inputController.OnRotateSword -= _sword.OnRotateSword;
        _inputController.OnGridingSword -= _sword.OnGridingSword;

        _inputController.OnSetSwordToHeating -= _sword.OnStarHeating;
        _inputController.OnStopSwordHeating -= _sword.OnStopHeating;

        _inputController.OnSetSwordToCooling -= _sword.OnStarCooling;
        _inputController.OnStopSwordCooling -= _sword.OnStopCooling;


        _inputController.OnDeliverSword -= _sword.DeliverSword;
        _sword = null;
    }
}
