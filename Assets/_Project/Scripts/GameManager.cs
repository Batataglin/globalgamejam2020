﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum ViewMode
{
    Furnace,
    WheelGrinder,
    Anvil, 
    AligmentView
}

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public delegate void SpawnHandler(Sword sword);
    public delegate float HeatUIHandler();

    public event SpawnHandler OnSpawn;
    public event HeatUIHandler OnUpdateHeatUI;

    [SerializeField] private bool _hardMode;

    [SerializeField] private GameObject _swordPrefab;
    [SerializeField] private Transform _swordSpawnPoint;
    [SerializeField] private Transform _swordParent;

    [SerializeField] private float _matchTime;
    [SerializeField] private int _lowSwordRate;

    [Header("UI")]
    [SerializeField] private Button _startButton;
    [SerializeField] private Button _restartButton;
    [SerializeField] private Toggle _hardcoreButton;
    [SerializeField] private GameObject _gameUI;
    [SerializeField] private GameObject _startPanel;
    [SerializeField] private GameObject _gameOverPanel;
    [SerializeField] private GameObject _keysPanel;
    [SerializeField] private TextMeshProUGUI _textTime;
    [SerializeField] private TextMeshProUGUI _textGameOver;
    [SerializeField] private Image _swordHeat;

    private bool _isSpawning;
    public ViewMode ViewMode { get; set; }
    public int SwordsRepaired { get; set; }
    public int GoodRepaired { get; set; }
    public int BadRepaired { get; set; }

    public bool HardMode { get => _hardMode; }

    private void Awake()
    {
        Instance = this;
        ViewMode = ViewMode.Anvil;

        _startButton.onClick.AddListener(StartGame);
        _restartButton.onClick.AddListener(StartGame);

        _startPanel.SetActive(true);
        _gameUI.SetActive(false);
        _gameOverPanel.SetActive(false);
        _keysPanel.SetActive(false);
        _hardcoreButton.isOn = false;

        _hardMode = _hardcoreButton.isOn;
        DontDestroyOnLoad(this);
    }

    public void SpawnSword()
    {
        if(_isSpawning)
        {
            return;
        }

        StartCoroutine(SpawnCoroutine());
    }
    private void StartGame()
    {
        SwordsRepaired = 0;
        GoodRepaired = 0;
        BadRepaired = 0;

        _startPanel.SetActive(false);
        _gameUI.SetActive(true);
        _keysPanel.SetActive(true);
        _gameOverPanel.SetActive(false);

        _hardMode = _hardcoreButton.isOn;

        if (_hasSword)
        {
            Destroy(_hasSword.gameObject);
        }

        StartCoroutine(GameTimer());
        SpawnSword();
    }

    private Sword _hasSword;

    private IEnumerator SpawnCoroutine()
    {
        _isSpawning = true;
        yield return new WaitForSeconds(1f);

        Sword sword = Instantiate(_swordPrefab, _swordSpawnPoint.position, _swordSpawnPoint.rotation).GetComponent<Sword>();

        sword.transform.SetParent(_swordParent);

        OnSpawn?.Invoke(sword);
        _hasSword = sword;
        _isSpawning = false;
    }

    private IEnumerator GameTimer()
    {
        float time = _matchTime * 60; 

        while (time > 0)
        {
            time -= Time.fixedDeltaTime;
            UpdateUI(ref time);

            yield return null;
        }
        GameOver();
    }

    private void UpdateUI(ref float time)
    {
        var ts = System.TimeSpan.FromSeconds(time);
        _textTime.text = string.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds);

        if (OnUpdateHeatUI != null)
        {
            _swordHeat.fillAmount = OnUpdateHeatUI.Invoke();
        }
    }

    private void GameOver()
    {
        _gameUI.SetActive(false);
        _gameOverPanel.SetActive(true);
        _keysPanel.SetActive(false);

        string finalMessage = string.Empty;

        // few sword
        if(SwordsRepaired < _lowSwordRate)
        {
            // Negative answer
            if (BadRepaired > GoodRepaired)
            {
                finalMessage = "Where is your loyalty!\n\n" +
                    "Your few swords were crocked and unwieldy. Not even the Gods could helped us on that battle." +
                    "The King want your head in a sitck by the end of the day.";
            }
            // Positive answer
            else if (GoodRepaired >= BadRepaired)
            {
                finalMessage = "Your swords were well balanced and easy to swing, but not enough for our amry.\n\n" +
                    "Our army was defeated!\n" +
                    "The King want your head in a sitck by the end of the day.";
            }
        }
        
        // enough swords
        else if (SwordsRepaired < _lowSwordRate + (_lowSwordRate / 2) && SwordsRepaired >= _lowSwordRate)
        {
            // Negative answer
            if (BadRepaired > GoodRepaired)
            {
                finalMessage = "Smith!\n\n" +
                    "You did the job, but the qualitty of your weapons were poor. " +
                    Random.Range(379, 2540) + " man died in battle due to your incompetence.\n" +
                    "You will keep your head with your shoulders!";
            }
            // Positive answer
            else if (GoodRepaired >= BadRepaired)
            {
                finalMessage = "That was a tight battle.\n\n" +
                    "As few as " + Random.Range(379, 1256) + " have perish in battle.\n" +
                    "You did your job, good work";
            }
        }

        // swords enough
        else if(SwordsRepaired >= _lowSwordRate + (_lowSwordRate / 2))
        {
            if (BadRepaired > GoodRepaired)
            {
                finalMessage = "We've come victorious this time.\n\n" +
                    "Your swords were crocked and unwieldy. But with the high quantity of swords, every man in the kingdom has a weapon to fight for the King.";
            }
            // Positive answer
            else if (GoodRepaired >= BadRepaired)
            {
                finalMessage = "Enjoy Smith, today we celebrate!\n\n" +
                    "Your effords help us to achieve the victory today. The swords were sharp and well balanced.\n" +
                    "The King have a place for you at the castle!";
            }
        }

        _textGameOver.text = finalMessage;
    }
}
